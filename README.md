# next-app-rh

**Tech stack**: _emacs, lsp-mode, tide-mode, node.js, next.js, react, yarn-pnp, yarn 3, typescript, typescript-language-server, eslint, prettier, @next/bundle-analyzer_

```next-app-rh``` is similar to ```next-app``` template that could be obtained by running:

```bash
yarn create next-app --typescript
```

with a few noticeable augmentations:
* Using Yarn 3 mono-repository making it easy to add more packages.
* Adding ```eslint-config-airbnb``` additionally to ```eslint-config-next``` and some custom ```eslint``` configurations.
* Providing ```.rh-project``` directory with a few useful project maintenance scripts.
* Providing Emacs IDE configuration.

# Emacs IDE configuration

Emacs IDE configuration is loaded when Emacs visits the project directory or one of its nested files. The loading process is bootstrapped via [.dir-locals.el](https://www.gnu.org/software/emacs/manual/html_node/emacs/Directory-Variables.html) file. The loading is performed by the ```rh-project-setup()``` function from [rh-project.el](https://github.com/ramblehead/.emacs.d/blob/master/lisp/rh-project.el) package.

```rh-project-setup()``` looks for the following files in the project's root: ```.rh-project/init.el``` and ```.rh-project/setup.el```. ```init.el``` is only loaded once per Emacs session while ```setup.el``` is loaded per visited file and directory.

# Usage instructions

```bash
git clone git@gitlab.com:ogorod/next-app-rh.git
cd next-app-rh
touch .rh-trusted # required by rh-project-setup() function
yarn
yarn app:build
yarn app:start

```

# Original .emacs.d

This project is targeting the following Emacs environment:

https://github.com/ramblehead/.emacs.d/

However, it should be possible to adapt for other Emacs environments.

# Screenshots

![](images/Screenshot_from_2021-10-23_00-17-40.png)
![](images/Screenshot_from_2021-10-23_00-23-30.png)
![](images/Screenshot_from_2021-10-23_00-33-36.png)
